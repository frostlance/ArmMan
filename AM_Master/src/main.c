#include "soc.h"
#include "gsm.h"

extern void Ble_Init( void );
extern void Ble_Power_Manage( void );
extern void Gsm_Sending( void );

static void initialization( void )
{
    Soc_Init();
    Ble_Init();
}


int main( void )
{
    initialization();
    Gsm_PowerOn();
    while(1) {
		Gsm_Sending();
        //Ble_Power_Manage();
    }
}

