
#include "gsm.h"
#include "soc.h"

const int8_t STR_OK[] = "OK\r\n";

extern void Control_Set_Led_Blink( uint8_t en );
volatile int8_t has_closed=0;
static RET_TYPE_T gsm_line_cmp( const int8_t *line_dst, const int8_t *line_src )
{
    uint32_t idx;
    for( idx=0;;idx++ ) {
        if( *(line_dst+idx) != *(line_src+idx) ) {
            return RET_FAILED;
        }
        if( *(line_dst+idx) == '\n' ) {
            return RET_OK;
        }
    }
}

static void gsm_line_waiting( const int8_t *line )
{
    static int8_t buff[UART_LINE_LENGTH_MAX];
    while(1) {
		Timer_Delay(100);
        if( Uart_Get_Line( buff ) == RET_OK ) {
			if( gsm_line_cmp( "CLOSED\r\n", buff ) ) {
				has_closed = 1;
			}
            if( gsm_line_cmp( line, buff ) == RET_OK ) {
                return ;
            }
        }
    }
}

static void gsm_at_step( const int8_t *input, const int8_t *output )
{
    Timer_Delay( 200 );
    Uart_Send_Line( input );
    gsm_line_waiting( output );
}

uint8_t old_key=0;


extern void wait_ble_stop( void );

extern void ble_restart_adv( void );
extern uint8_t ble_get_rssi( void );

void Gsm_PowerOn( void )
{
    Control_Set_Led_Blink( 1 );

    /* turn on gsm power */
    Ttl_Io_Set( PIN_GSM_POWER_EN, 1);
    Ttl_Io_Set( PIN_GSM_POWER_KEY, 0);
    Timer_Delay( 1000 );
    /* set gsm power key sequence */
    //Ttl_Io_Set( PIN_LED_RED, 1 );
    Ttl_Io_Set( PIN_GSM_POWER_KEY, 1);
    Timer_Delay( 2000 );
    Ttl_Io_Set( PIN_GSM_POWER_KEY, 0);
    //Ttl_Io_Set( PIN_LED_RED, 0 );

    /*  wait recieving "CREG" string  */
//    while( 1 ) {
//        if( gsm_line_cmp( "+CGREG: 1\r\n",  ) ) {
//            break;
//        }
//    }
    Uart_Init();
    Timer_Delay( 15000 );
    Uart_Send_Line( "AT+GPSACT=1\r\n" );
		Control_Set_Led_Blink( 0 );
	old_key = Ttl_Io_Get( PIN_KEY2 );
	return;
		wait_ble_stop();
    Timer_Delay( 100 );

    gsm_line_waiting( "+CGREG: 1\r\n" );
    //Ttl_Io_Set( PIN_LED_RED, 1 );

    gsm_at_step( "AT+CGATT=1\r\n", STR_OK );
    gsm_at_step( "AT+CGDCONT=1,\"IP\",\"CMMTM\"\r\n", STR_OK );
    gsm_at_step( "AT+CGACT=1,1\r\n", STR_OK );
    gsm_at_step( "AT+CIFSR\r\n", STR_OK );
    gsm_at_step( "AT+CIPSTATUS\r\n", STR_OK );
    gsm_at_step( "AT+CIPSTART=\"TCP\",\"183.230.40.40\",1811\r\n", STR_OK );

    Uart_Send_Line( "AT+CIPSEND\r\n" );
    Timer_Delay( 500 );
    Uart_Send_Line( "*173609#test#sample*\x1a" );
    Timer_Delay( 500 );
    Uart_Send_Line( "AT+CIPSEND\r\n" );
    Timer_Delay( 500 );
    Uart_Send_Line( "Start\x1a" );
    Timer_Delay( 500 );
    Uart_Send_Line( "AT+CIPCLOSE\r\n" );
    Timer_Delay( 500 );
    Uart_Send_Line( "AT+CIPSHUT\r\n" );


    Timer_Delay( 500 );
    Uart_Send_Line( "AT+GPSACT=1\r\n" );
		Control_Set_Led_Blink( 0 );
	has_closed = 0;
	old_key = Ttl_Io_Get( PIN_KEY2 );
	ble_restart_adv();
	return;

//    while(1) {
//    gsm_at_step( "AT+CIPSTART=\"TCP\",\"183.230.40.40\",1811\r\n", STR_OK );
//    Uart_Send_Line( "AT+CIPSEND\r\n" );
//    Timer_Delay( 500 );
//    Uart_Send_Line( "aaa\x1a" );
//    }
//    /* done */
}

void Gsm_Sending()
{
	uint32_t i;
	static int8_t gps_data[256];
	static int8_t tmp_data[256];
	static uint32_t times=0;
	uint8_t rssi;
	if( old_key == Ttl_Io_Get( PIN_KEY2 ) ) {
		return;
	}
//    if( !old_key ) {
//        return;
//    }
//	if( Ttl_Io_Get( PIN_KEY2 ) ) {
//		return;
//	}
	rssi  = ble_get_rssi();
	Control_Set_Led_Blink( 1 );
		wait_ble_stop();
    Timer_Delay( 300 );
		gsm_at_step( "AT+CGATT=1\r\n",STR_OK );
	  Timer_Delay( 300 );
		gsm_at_step( "AT+CGDCONT=1,\"IP\",\"CMMTM\"\r\n", STR_OK );
	  Timer_Delay( 300 );
		gsm_at_step( "AT+CGACT=1,1\r\n", STR_OK );
	  Timer_Delay( 300 );
		gsm_at_step( "AT+CIFSR\r\n", STR_OK );
	  Timer_Delay( 300 );
		gsm_at_step( "AT+CIPSTATUS\r\n", STR_OK );
	  Timer_Delay( 300 );
	  while( Uart_Get_Line(gps_data) == RET_OK );
    Uart_Send_Line( "AT+GPSPOS\r\n" );
	  while( 1 ) {
			//Timer_Delay( 50 );
	    Uart_Get_Line( gps_data );
			if( gps_data[0] == '+' &&
						gps_data[1] == 'G' && 
						gps_data[2] == 'P' && 
						gps_data[3] == 'S' ) {
							break;
						}
		}
		for( i =0; gps_data[i]; i++ ) {
//			if( gps_data[i] == ',' ) {
//				gps_data[i] = ' ';
//			}
			if( gps_data[i] == 0x0d ) {
				gps_data[i] = 0x00;
			}
			if( gps_data[i] == 0x0a ) {
				gps_data[i] = 0x00;
			}
		}

		gsm_at_step( "AT+CIPSTART=\"TCP\",\"183.230.40.40\",1811\r\n", STR_OK );
		Timer_Delay( 1500 );

		Uart_Send_Line( "AT+CIPSEND\r\n" );
		Timer_Delay( 500 );
		gsm_at_step( "*173609#test#sample*\x1a", STR_OK );
//	}
		Timer_Delay( 500 );
	  while( Uart_Get_Line(tmp_data) == RET_OK );
		Uart_Send_Line( "AT+CIPSEND\r\n" );
	  while( 1 ) {
			//Timer_Delay( 50 );
	    Uart_Get_Line( tmp_data );
			if( tmp_data[0] == '>' ) {
				break;
			}
		}
	//Timer_Delay( 1000 );
//	Uart_Send_Line( "{" );
//	Uart_Send_Line( "asdlfkjalsdjfkajsdf"  );	
	Uart_Send_Line( gps_data+9 );	
//	Uart_Send_Line( "0 0.0000 N 0.0000 E 0.0 33" );	
	Uart_Send_Line( ",rssi %d", rssi );
	Uart_Send_Line( "\x1a" );
	
		Timer_Delay( 1500 );
    Uart_Send_Line( "AT+CIPCLOSE\r\n" );
    Timer_Delay( 500 );
    Uart_Send_Line( "AT+CIPSHUT\r\n" );



		Control_Set_Led_Blink( 0 );
	has_closed = 0;
    old_key = Ttl_Io_Get( PIN_KEY2 );
	ble_restart_adv();
}



/**
 * Gsm Module Services
 */

void Gsm_Get_Atcmd_Status( void *status )
{

}

void Gsm_Send_Atcmd( int8_t *cmd )
{

}
