
#ifndef __SOC_H__
#define __SOC_H__
#include "common_type.h"

#include "nrf_drv_gpiote.h"
#include "nrf_drv_uart.h"
#include "nrf_assert.h"
#include "nrf_drv_common.h"
#include "app_uart.h"

#define UART_LINE_LENGTH_MAX        256
#define UART_LINE_FIFO_MAX          16

typedef enum ttl_io_t
{
    PIN_GSM_POWER_EN = 5,
    PIN_GSM_POWER_KEY = 30,
    PIN_GNSS_POWER_EN = 6,
    PIN_LED_RED = 14,
    PIN_LED_BLUE = 15,
    PIN_KEY1 = 21,
    PIN_KEY2 = 19,
    PIN_CAM_EN = 24,
	PIN_HALL_POWER = 20,
} TTL_IO_T;

void Ttl_Io_Init( void );
void Ttl_Io_Set( TTL_IO_T pin, uint8_t set );
uint8_t Ttl_Io_Get( TTL_IO_T pin );
void Uart_Init( void );
void Uart_Send_Line( const int8_t *line, ... );
RET_TYPE_T Uart_Get_Line( int8_t *line );
void Timer_Delay( uint32_t ms );


void Soc_Init( void );



#endif //__SOC_H__
