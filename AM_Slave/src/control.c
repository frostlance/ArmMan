

#include "sdk_common.h"

#include "nrf_drv_gpiote.h"
#include "ble_nus.h"

#define POWER_KEY_IO_NO         3
#define POWER_ON_IO_NO          4
#define LED_ON_IO_NO            29
#define BEE_NO					28

extern ble_nus_t                        m_nus;

static void power_key_event_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    switch( pin ) {
        case POWER_KEY_IO_NO:
            ble_nus_string_send( &m_nus, "asdf", 4 );
            break;
        default:
            break;
    }
}

void control_io_init( void )
{
    nrf_drv_gpiote_in_config_t cfg = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);

   if (!nrf_drv_gpiote_is_init())
    {
        nrf_drv_gpiote_init();
    }

    nrf_drv_gpiote_in_init( POWER_KEY_IO_NO, &cfg, power_key_event_handler );
    nrf_drv_gpiote_in_event_enable(POWER_KEY_IO_NO, true);

    nrf_gpio_cfg_output( POWER_ON_IO_NO );
    nrf_gpio_cfg_output( LED_ON_IO_NO );
    nrf_gpio_pin_clear( POWER_ON_IO_NO );
    nrf_gpio_pin_set( LED_ON_IO_NO );
	
	nrf_gpio_cfg_output( BEE_NO );
}

