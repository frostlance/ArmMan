#ifndef __COMMON_TYPED_H
#define __COMMON_TYPED_H
#include <stdint.h>

typedef enum ret_type_t
{
    RET_OK = 0,
    RET_FAILED,
} RET_TYPE_T;


#endif //__COMMON_TYPED_H
