
#Project Summary
***
##Description
  * ArmMan是一个物品管理的项目, 将物品的位置信息,以及物品的近点距离报警信息发送置服务器.
  * 分为一个Master模块,一个Slave模块.
    1. Master模块: 包括GSM,GNSS,BLE Central. 将GNSS经纬度,BLE 测距信息 通过GSM发送至服务器.
    2. Slave模块: BLE peripheral. 与Master模块通过BLE建立连接,用于距离检测.

##Reference
  * https://developer.nordicsemi.com/ 各版本SDK下载地址

##Category
1. [AM_Master](./am_master.md)
2. [AM_Slave](./am_slave.md)
3. [Misc](./misc.md)
