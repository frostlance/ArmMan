
## 配对过程
   静态密码配对,密码暂设"12345".

## 连接方式
   串口透传

## 测距方式
   根据 rssi测距, 将rssi值转换为uint8_t类型的距离值distance.
   distance数值为(0-255),
   越接近主机,则distance值越小
   越远离,则distance值越大,10米为100,若信号中断,值为255.

## 主从交互

#### 数据格式
```C
{
    //主机发送的数据包
    struct master_transdata
    {
        uint32_t current_master_ms;     //当前的主机时间,毫秒
        uint32_t next_start_ms;         //下次侦测窗口的开始时间,毫秒
        uint32_t next_stop_ms;          //下次侦测窗口的结束时间,毫秒
    }

    //从机回应的数据包
    struct slave_transdata
    {
        uint8_t distance;               //通过rssi转换的距离值
    }
}

#### 主机
  主机每隔一段时间会开启蓝牙,例如每隔5秒会有1秒的开启时间.这1秒就称为侦测窗口
  如果主机在侦测窗口开启时,与从机连接成功,那么就会发送一个master_transdata包
  master_transdata包定义了下次侦测窗口开启的时间.
  主机等待接收从机的slave_transdata数据包，收到后断开连接。

### 从机
  从机开机后,直接开启广播
  如果与主机连接上后,则获取master_transdata数据包，并发送salve_transdata。
  从机随后可以根据侦测窗口的时间，来对应开启或者关闭广播，可节省功耗。
  当然如果自身功耗能够保证，可以一直开启广播。
   