
# Am_Master
***


## Hardware configuration
* nRF51822, 带有cortex-m3内核的ble芯片
* m6220，中移动2G通信模块，其中包含gsm,gnss,esim．


## Code Templet
* nRF5_SDK_12.3.0\examples\ble_central\ble_app_uart_c


## Memory configuration
* 与例程相同，无特殊配置。


## Programing summary
* 由于nordic SoftDevice的事件驱动的使用方式，使用代码如果出现阻塞则会出现各种问题．利用Timer建立数个周期执行的状态机，可以使整个代码以无阻塞的方式运行．
* 整个程序可以看作是数个任务在并行运行：
    * [主程序](#main)
    * [硬件中断服务程序](#irq)
    * [hardware状态机](#hardware_sm)
    * [gsm状态机](#gsm_sm)
    * [controller状态机](#controller状态机)

* 任务之间的通信
    * 使用queque的方式
    * 访问queue时，务必进行临界区保护
        * sd_nvic_critical_region_enter 进入临界区
        * sd_nvic_critical_region_exit 离开临界区
        * 详见SDK中"nrf_nvic.h"

<span id= "main">

* 主程序
    1. 初始化硬件接口，注意，需要将m6220模块上电之后再初始化uart，否则无法启动
    2. 初始化queue，用于各任务之间通信
        * peripheral_queue, 用于底层硬件消息传输
        * control_queue_rx, control_queue_tx，用于硬件逻辑交互
        * gsm_queue，用于gsm at commands 的传输
    3. 循环低功耗休眠

<span id= "irq">

* 硬件中断服务程序
    * 获取硬件信息
    * 将信息写入peripheral_queue中

<span id= "hardware_sm" >

* hardware状态机
    *　从peripheral_queue中获取信息．
    * 经过处理后通过control_queue_rx, control_queue_tx与逻辑控制部分交互

<span id= "gsm_sm" >

* gsm状态机
```c
    //伪代码
    //gsm_queue数据结构
    struct gsm_queue_sturct
    {
        // ATCommand 命令行
        int8_t at_command_line[];
        // 执行完成后并成功的标志性字符串
        int8_t return_success_line[];
        // 执行完成并失败得标志性字符串
        int8_t return_failed_line[];
        // 超时设置时间ms
        uint32_t timeout_ms;
    }
    gms_state_machine()
    {
        static uint8_t state;
        switch( state ) 
        {
            case idle:          //空闲状态
                if( /*gsm_queue中有信息*/ ) {
                    state = uart_send;
                }
                break;
            
            case uart_send:     //uart发送状态
                /* 通过Uart发送gsm_queue_sturct.at_command_line*/
                state = uart_wait_answer;
                break;

            case uart_wait_answer:
                if(/* 如果等待到　gsm_queue_sturct.return_success_line */ ) {
                    state = idle;
                }
                if(/* 如果等待到　gsm_queue_sturct.return_failed_line*/) {
                    state = failed;
                }
                if( /*超时*/ ) {
                    state = timeout;
                }
                break;

            case uart_wait_failed:
                /*出错处理*/
                break;

            case uart_wait_timeout:
                /*超时处理*/
                state = idle;
                break;
                
        }
    }
```