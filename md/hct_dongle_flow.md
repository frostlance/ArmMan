
#主流程
```flow 
st=>start: 开始 
init1=>subroutine: MCU端口初始化
init2=>subroutine: IAP2通信协议初始化
init3=>subroutine: USB host端口初始化
service1=>subroutine: IAP2通信服务
service2=>subroutine: USB通信服务
service3=>subroutine: 人机控制接口流程

st->init1->init2->init3(right)->service1->service2(right)->service3->service1
```

#MCU端口初始化
```flow
st=>start: mcu端口初始化
end=>end: 初始化完成
op1=>operation: 初始化 IO口扩展74hc595
op2=>operation: 初始化LED,KEY,充电器插入检测口，iOS设备插入检测口
op3=>operation: 初始化ADC，包括充电电流，电池电压
op4=>operation: 初始化Uart，用于调试信息输出

st->op1->op2->op3->op4->end
```

#IAP2通信协议初始化
```flow
st=>start: IAP2通信协议初始化
end=>end: 初始化完成
op1=>operation: 初始化I2C通信，用于Apple Mfi 认证芯片通信
op2=>operation: 初始化USB device接口，用于与Lightning接口连接
op3=>operation: 创建IAP2实例，分配内存。

st->op1->op2->op3->end
```

#USB host 端口初始化
```flow
st=>start: USB host 端口初始化
end=>end: 初始化完成
op1=>operation: 初始化spi，用于ch374 USB host 芯片
op2=>operation: ch374 上电
op3=>operation: 初始化ch374协议，并启动

st->op1->op2->op3->end
```

#IAP2通信服务
```flow
st=>start: IAP2通信服务状态机
end=>end: 结束
con1=>condition: 是否认证通过
con2=>condition: 是否插入iOS设备
con3=>condition: 是否有接收到iOS设备消息
op1=>operation: 检测认证芯片。
op2=>operation: 开启设备与iOS设备之间的IAP2连接
op3=>operation: 处理ios设备消息

st->con1
con1(yes)->con2
con1(no)->op1->end
con2(yes)->op2->con3
con2(no)->end
con3(yes)->op3->end
con3(no)->end
```

#USB通信服务
```flow
st=>start: USB通信服务状态机
end=>end: 结束
con1=>condition: 有设备插入
con2=>condition: 进入低功耗状态
op1=>operation: 初始化设备
op2=>operation: 低功耗模式
op3=>operation: 接收usb设备信息，将其放入FIFO，供人机接口使用

st->con1
con1(no)->end
con1(yes)->op1->con2
con2(yes)->op2->end
con2(no)->op3->end
```

#人机控制接口流程
```flow
st=>start: 人机接口流程
end=>end: 结束
con1=>condition: 是否有按键
con11=>condition: 开关机按键
con12=>condition: 缩放调整按键
op11=>operation: 开关机
op12=>operation: 缩放

con2=>condition: 是否有USB位移信息
op2=>operation: 发送位移

st->con1
con1(yes,right)->con11
con11(yes,right)->op11
con11(no)->con12
con12(yes,right)->op12
con12(no)->end

con1(no)->con2
con2(yes,right)->op2->end
con2(no)->end
```
