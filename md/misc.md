# nRF51822 随记

## Record
1. "sd_***"开头的api为softDevice API,在文档中的对应版本softDevice中查找
2. "nrf_***"开头的api为SDK API,在文档中的SDK对应版本中查找
## Trouble
1. 在vmware虚拟机下烧录softDevice有问题,会导致不断重启的状况,换到主系统上就没有此问题
2. 连接M6220模块时, 需要先把M6220模块的电源打开,之后再初始化nRF51822的串口,否则会导致nRF51822无法正常启动.
3. [Ble连接后自动断开的问题](#Ble连接后自动断开的问题)
4. 将hrs_peripheral的freeRTOS例程转移到UART_Central例程会出现不明重启的状况,无精力去debug,弃用





***
###### Ble连接后自动断开的问题
* 问题点: Am_master与Am_slave通过Ble连接之后,  如果Am_master通过Uart连续发送GSM At command到m6220模块, 会导致Ble连接自动断开.
* 解决: 尚未
